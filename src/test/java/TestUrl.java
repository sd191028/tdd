import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestUrl {
  @Test
  public void constructor() {
    Url u = new Url("https://launchcode.org/learn");
    assertEquals("https", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn", u.getPath());
  }

  @Test
  public void constructorNoPath() {
    Url u = new Url("https://launchcode.org/");
    assertEquals("https", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/", u.getPath());
  }

  @Test
  public void constructorExtraSlash() {
    Url u = new Url("https://launchcode.org/learn/");
    assertEquals("https", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn", u.getPath());
  }

  @Test
  public void constructorExtraDoubleSlashes() {
    Url u = new Url("https://launchcode.org/learn//a");
    assertEquals("https", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn/a", u.getPath());
  }

  @Test
  public void constructorWithPort() {
    Url u = new Url("http:8080//launchcode.org/learn");
    assertEquals("http", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn", u.getPath());
  }

  @Test
  public void constructorLongPath() {
    Url u = new Url("http://launchcode.org/learn/a/b/c/123.html");
    assertEquals("http", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn/a/b/c/123.html", u.getPath());
  }

  @Test
  public void constructorLowerCase() {
    Url u = new Url("htTps://LAUNCHCODE.org/leArN");
    assertEquals("https", u.getProtocol());
    assertEquals("launchcode.org", u.getDomain());
    assertEquals("/learn", u.getPath());
  }

  @Test
  public void properSimpleToString() {
    Url u = new Url("https://launchcode.org/learn");
    assertEquals("https://launchcode.org/learn", u.toString());
  }

  @Test
  public void properExtraSlashToString() {
    Url u = new Url("https://launchcode.org/learn/");
    assertEquals("https://launchcode.org/learn", u.toString());
  }

  @Test
  public void constructorExtraDoubleSlashesToString() {
    Url u = new Url("https://launchcode.org/learn//a");
    assertEquals("https://launchcode.org/learn/a", u.toString());
  }

  @Test
  public void allowedProtocols() {
    Url u;
    u = new Url("ftp://launchcode.org");
    assertEquals("ftp://launchcode.org/", u.toString());
    u = new Url("http://launchcode.org");
    assertEquals("http://launchcode.org/", u.toString());
    u = new Url("https://launchcode.org");
    assertEquals("https://launchcode.org/", u.toString());
    u = new Url("file://launchcode.org");
    assertEquals("file://launchcode.org/", u.toString());
  }

  @Test(expected = IllegalArgumentException.class)
  public void exceptionBadProtocol() {
    Url u = new Url("htt://launchcode.org/learn/");
    fail("Should not accept bad protocol.");
  }

  @Test(expected = IllegalArgumentException.class)
  public void exceptionNoProtocol() {
    Url u = new Url("");
    fail("Should not accept missing protocol.");
  }

  @Test(expected = IllegalArgumentException.class)
  public void exceptionNoDomain() {
    Url u = new Url("http://");
    fail("Should not accept missing domain.");
  }

  @Test(expected = IllegalArgumentException.class)
  public void exceptionInvalidDomainChars() {
    Url u = new Url("http://launch$code.org");
    fail("Should not accept invalid domain.");
  }

  @Test
  public void validDomainChars() {
    Url u = new Url("https://launch-code_Test54.org");
    assertEquals("https://launch-code_test54.org/", u.toString());
  }
}
