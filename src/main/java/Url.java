public class Url {
  private final String protocol;
  private final String domain;
  private final String path;

  public final String[] allowedProtocols = {"file", "ftp", "http", "https"};

  public boolean isAllowedProtocol(String val) {
    for (String allowedProtocol : allowedProtocols) {
      if (val.equals(allowedProtocol)) {
        return true;
      }
    }
    return false;
  }

  public Url(String url) {
    url = url.toLowerCase();

    // Pull out protocol
    String[] protocolSections = url.split(":");
    if (protocolSections.length == 0) {
      throw new IllegalArgumentException("Empty protocol.");
    }

    // Verify protocol
    if (!isAllowedProtocol(protocolSections[0])) {
      throw new IllegalArgumentException("Disallowed protocol.");
    }

    // Set protocol
    protocol = protocolSections[0];

    // Pull out sections by forward slash
    String[] sections = url.split("/");
    // Walk through empty slashes
    int i = 1;
    for (; i < sections.length; i++) {
      if (sections[i].equals("")) {
        continue;
      }
      break;
    }

    // Verify domain
    if (sections.length == i) {
      throw new IllegalArgumentException("Empty domain.");
    }
    if (!sections[i].matches("[A-Za-z0-9._-]*")) {
      throw new IllegalArgumentException("Invalid characters in domain.");
    }

    // Set domain
    domain = sections[i];

    // Assign remaining portions of url to path
    i++;
    if (i == sections.length) {
      path = "/";
    } else {
      StringBuilder s = new StringBuilder();
      for (; i < sections.length; i++) {
        if (sections[i].equals("")) {
          continue;
        }
        s.append("/");
        s.append(sections[i]);
      }
      path = s.toString();
    }
  }

  public String getProtocol() {
    return protocol;
  }

  public String getDomain() {
    return domain;
  }

  public String getPath() {
    return path;
  }

  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append(this.getProtocol());
    s.append("://");
    s.append(this.getDomain());
    s.append(this.getPath());
    return s.toString();
  }
}
